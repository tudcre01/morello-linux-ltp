Linux Test Project for Morello
==============================

Morello LTP, project enrolled as a fork of the upstream `Linux Test Project
<http://linux-test-project.github.io/>`_, aims at enabling LTP with architectural
capabilities based on `CHERI <https://www.cl.cam.ac.uk/research/security/ctsrd/cheri/>`_
model (and as provided by `Morello <https://www.arm.com/architecture/cpu/morello>`_
architecture). Independently developed from its original project, Morello LTP
places its focus on verifying and validating capabilities support within the
`(Morello) Linux kernel <https://git.morello-project.org/morello/kernel/linux>`_
[#]_ and, at this point, targets *kernel syscalls testcases* only. Due to
external limitations only static builds are supported.

Discussions, contributions and reporting issues conveyed on the mailing list:
https://op-lists.linaro.org/mailman3/lists/linux-morello-ltp.op-lists.linaro.org

.. [#] Mainline-based fork of the Linux kernel for Morello supporting
       pure-capability kernel-user ABI (PCuABI)
       (see `Morello Linux ABI specifications <https://git.morello-project.org/morello/kernel/linux/-/wikis/home>`_)

Building (cross-compiling) & running relevant tests (purecap mode)
-------------------------------------------------------------------

Dependencies/requirements: (additional to what's required to build base LTP):

- Morello LLVM toolchain (version >= 1.4)

  Sources: https://git.morello-project.org/morello/llvm-project

  Binary releases: https://git.morello-project.org/morello/llvm-project-releases

    - **morello/baremetal-release-1.4** + **morello/linux-aarch64-release-1.4** for Morello
      purecap crt objects


- Musl libc for Morello (version >= 1.4): https://git.morello-project.org/morello/musl-libc

- Kernel uAPI header files: https://git.morello-project.org/morello/morello-linux-headers

Building Musl:
--------------

Assumptions:

.. code-block::

   clang - Morello Clang (see Dependencies) configured for current environment

.. code-block::


    CC=clang ./configure \
    --disable-shared \
    --enable-morello \
    --disable-libshim \
    --target=aarch64-linux-musl_purecap \
    --prefix=${INSTALL_PATH}


.. code-block::

    make
    make install


For more details see `Musl libc for Morello README <https://git.morello-project.org/morello/musl-libc/-/blob/morello/master/README.rst>`_.

Building LTP syscall testcases:
-------------------------------
Assumptions:

.. code-block::

    clang            - Morello Clang (see Dependencies) configured for current environment
    KHDR_DIR         - kernel uAPI headers
    MUSL	     - Musl build (see above)
    MORELLO_CRT_OBJS - as per https://git.morello-project.org/morello/llvm-project-releases/-/tree/morello/linux-aarch64-release-1.4
                       -> $(clang -print-resource-dir)/lib/aarch64-unknown-linux-musl_purecap


.. code-block::

    CLANG_RES_DIR=$(clang -print-resource-dir)

    CFLAGS_COMMON="--target=aarch64-linux-gnu -integrated-as -g -static -nostdinc -pthread \
    -isystem $KHDR_DIR/usr/include"

    CFLAGS_PURECAP="$CFLAGS_COMMON -march=morello+c64 -mabi=purecap \
    -isystem $CLANG_RES_DIR/include -isystem $MUSL/include -Wall"

    LDFLAGS_COMMON="--target=aarch64-linux-gnu -fuse-ld=lld -static -nostdlib"

    LDFLAGS_PURECAP="$LDFLAGS_COMMON $MUSL/lib/crt1.o $MUSL/lib/crti.o \
    $MORELLO_CRT_OBJS/clang_rt.crtbegin.o $MORELLO_CRT_OBJS/libclang_rt.builtins.a \
    $MORELLO_CRT_OBJS/clang_rt.crtend.o $MUSL/lib/crtn.o -L$MUSL/lib/ -lc -L$LTP_BUILD/lib"

    export CC=clang
    export HOST_CFLAGS="-O2 -Wall"
    export HOST_LDFLAGS="-Wall"
    export CONFIGURE_OPT_EXTRA="--prefix=/ --host=aarch64-linux-gnu --disable-metadata --without-numa"

    MAKE_OPTS="TST_NEWER_64_SYSCALL=no TST_COMPAT_16_SYSCALL=no" \
    TARGETS="pan testcases/kernel/syscalls" BUILD_DIR="$LTP_BUILD" \
    CFLAGS="$CFLAGS_PURECAP" LDFLAGS="$LDFLAGS_PURECAP" \
    ./build.sh -t cross -o out -ip "${OUTPUT_DIR}/opt/ltp"


Running tests:
--------------
.. code-block::

    runltp -f morello_transitional,morello_transitional_extended


Developers playground
---------------------

First things first:  read the docs - LTP rules/guidelines still apply (see section below).
Plus there are number of resources linked throughout this document; when in doubt.
If there is still something missing, `Morello LTP mailing list <https://op-lists.linaro.org/mailman3/lists/linux-morello-ltp.op-lists.linaro.org>`_
is the right place to go.

The above applies to all contributions and issue reports.
Pull requests should give place to appropriate patch submission on the mailing list.

Note on *next*
~~~~~~~~~~~~~~~~
While working on ``morello/next`` be mindful of the dependencies (see ``'Building'``
section). Some testcases might require newer version of either (or all) of them.
This should, preferably, be clearly stated for each testcase affected.


Original README / Further reading
---------------------------------
Original Linux Test Project `README <README-LTP.md>`_ file.
